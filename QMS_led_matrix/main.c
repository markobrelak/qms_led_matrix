/*
 * QMS_led_matrix.c
 *
 * Created: 4/11/2015 8:44:25 PM
 * Author : Brelak
 */ 

#define F_CPU 8000000UL
#define BAUD  9600

#include <util/delay.h>
#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include <stdbool.h>
#include <avr/pgmspace.h>

#define _11_LEDS

#ifdef _9_LEDS
	///////// Font related customization //////////
	#define		charWidth	5							// Width of the character
	#define		charHeigthBigFont	9					// Character height
	#define		frameWidth	23							// Width of the display
	#define		displayHeigth	11						// Height of the character
	#define		charHeightSmallFont	5					// Small font character height
	#define		big_font_offset	1						// Height offset for big font
	#define		small_font_first_row_offset		0		// Height offset for small font first row
	#define		small_font_second_row_offset	6		// Height offset for small font second row
	
	////////// Big font ///////////
	/////////// 9 LED /////////////
	const int A[] PROGMEM = {508, 34, 33, 34, 508};
	const int B[] PROGMEM = {511, 273, 281, 278, 224};
	const int C[] PROGMEM = {254, 257, 257, 257, 198};
	const int D[] PROGMEM = {511, 257, 257, 130, 124};
	const int E[] PROGMEM = {511, 273, 273, 273, 257};
	const int F[] PROGMEM = {511, 9, 9, 9, 1};
	const int G[] PROGMEM = {254, 257, 273, 273, 246};
	const int H[] PROGMEM = {511, 16, 16, 16, 511};
	const int I[] PROGMEM = {0, 257, 511, 257, 0};
	const int J[] PROGMEM = {193, 257, 257, 257, 255};
	const int K[] PROGMEM = {511, 16, 40, 71, 384};
	const int L[] PROGMEM = {511, 256, 256, 256, 0};
	const int M[] PROGMEM = {511, 4, 8, 4, 511};
	const int N[] PROGMEM = {511, 8, 16, 32, 511};
	const int O[] PROGMEM = {254, 257, 257, 257, 254};
	const int P[] PROGMEM = {511, 17, 17, 17, 14};
	const int Q[] PROGMEM = {254, 257, 321, 385, 254};
	const int R[] PROGMEM = {511, 17, 49, 81, 398};
	const int S[] PROGMEM = {270, 273, 273, 273, 225};
	const int T[] PROGMEM = {1, 1, 511, 1, 1};
	const int U[] PROGMEM = {255, 256, 256, 256, 255};
	const int V[] PROGMEM = {127, 128, 256, 128, 127};
	const int W[] PROGMEM = {255, 256, 224, 256, 255};
	const int X[] PROGMEM = {455, 40, 16, 40, 455};
	const int Y[] PROGMEM = {7, 8, 496, 8, 7};
	const int Z[] PROGMEM = {449, 289, 273, 265, 263};

	const int _0[] PROGMEM = {254, 289, 273, 265, 254};
	const int _1[] PROGMEM = {260, 258, 511, 256, 256};
	const int _2[] PROGMEM = {390, 321, 289, 273, 270};
	const int _3[] PROGMEM = {130, 257, 273, 273, 238};
	const int _4[] PROGMEM = {56, 36, 34, 511, 32};
	const int _5[] PROGMEM = {143, 265, 265, 265, 241};
	const int _6[] PROGMEM = {254, 273, 273, 273, 230};
	const int _7[] PROGMEM = {1, 497, 9, 5, 3};
	const int _8[] PROGMEM = {238, 273, 273, 273, 238};
	const int _9[] PROGMEM = {142, 273, 273, 273, 254};
#endif

#ifdef _11_LEDS
	///////// Font related customization //////////
	#define		charWidth	5							// Width of the character
	#define		charHeigthBigFont	11					// Character height
	#define		frameWidth	23							// Width of the display
	#define		displayHeigth	11						// Height of the character
	#define		charHeightSmallFont	5					// Small font character height
	#define		big_font_offset	0						// Height offset for big font
	#define		small_font_first_row_offset		0		// Height offset for small font first row
	#define		small_font_second_row_offset	6		// Height offset for small font second row

	////////// Big font ///////////
	/////////// 11 LED ////////////
	const int A[] PROGMEM = {2044, 66, 65, 66, 2044};
	const int B[] PROGMEM = {2047, 1057, 1057, 1057, 990};
	const int C[] PROGMEM = {1022, 1025, 1025, 1025, 774};
	const int D[] PROGMEM = {2047, 1025, 1025, 1025, 1022};
	const int E[] PROGMEM = {2047, 1057, 1057, 1057, 1057};
	const int F[] PROGMEM = {2047, 33, 33, 33, 33};
	const int G[] PROGMEM = {1022, 1025, 1089, 1089, 966};
	const int H[] PROGMEM = {2047, 32, 32, 32, 2047};
	const int I[] PROGMEM = {0, 1025, 2047, 1025, 0};
	const int J[] PROGMEM = {769, 1025, 1025, 1025, 1023};
	const int K[] PROGMEM = {2047, 192, 304, 524, 1027};
	const int L[] PROGMEM = {2047, 1024, 1024, 1024, 1024};
	const int M[] PROGMEM = {2047, 2, 4, 2, 2047};
	const int N[] PROGMEM = {2047, 16, 32, 64, 2047};
	const int O[] PROGMEM = {1022, 1025, 1025, 1025, 1022};
	const int P[] PROGMEM = {2047, 33, 33, 33, 30};
	const int Q[] PROGMEM = {1022, 1025, 1281, 513, 1534};
	const int R[] PROGMEM = {2047, 97, 161, 289, 1566};
	const int S[] PROGMEM = {542, 1057, 1057, 1057, 962};
	const int T[] PROGMEM = {1, 1, 2047, 1, 1};
	const int U[] PROGMEM = {1023, 1024, 1024, 1024, 1023};
	const int W[] PROGMEM = {511, 1536, 480, 1536, 511};
	const int V[] PROGMEM = {511, 512, 1024, 512, 511};
	const int X[] PROGMEM = {1799, 216, 32, 216, 1799};
	const int Y[] PROGMEM = {63, 64, 1920, 64, 63};
	const int Z[] PROGMEM = {1921, 1089, 1057, 1041, 1039};
	const int _1[] PROGMEM = {4, 1026, 2047, 1024, 0};
	const int _2[] PROGMEM = {1794, 1153, 1089, 1057, 1054};
	const int _3[] PROGMEM = {514, 1025, 1057, 1057, 990};
	const int _4[] PROGMEM = {112, 72, 68, 66, 2047};
	const int _5[] PROGMEM = {575, 1041, 1041, 1041, 993};
	const int _6[] PROGMEM = {1022, 1057, 1057, 1057, 962};
	const int _7[] PROGMEM = {1, 1985, 33, 17, 15};
	const int _8[] PROGMEM = {990, 1057, 1057, 1057, 990};
	const int _9[] PROGMEM = {542, 1057, 1057, 1057, 1022};
	const int _0[] PROGMEM = {1022, 1089, 1057, 1041, 1022};
#endif

const int rightArrow[] PROGMEM = {0, 124, 56, 16, 0};
const int leftArrow[] PROGMEM = {0, 16, 56, 124, 0};
const int upArrow[] PROGMEM = {32, 48, 56, 48, 32};
const int downArrow[] PROGMEM = {16, 48, 112, 48, 16};

const int space[] PROGMEM = {0, 0, 0, 0, 0};
///////////////////////////////

///////// Small font //////////
/////////// 9 LED /////////////
const int As[] PROGMEM = {30, 9, 30, 0, 0};
const int Bs[] PROGMEM = {31, 21, 27, 0, 0};
const int Cs[] PROGMEM = {31, 17, 27, 0, 0};
const int Ds[] PROGMEM = {31, 17, 14, 0, 0};
const int Es[] PROGMEM = {31, 21, 17, 0, 0};
const int Fs[] PROGMEM = {31, 5, 1, 0, 0};
const int Gs[] PROGMEM = {31, 17, 21, 29, 0};
const int Hs[] PROGMEM = {31, 4, 31, 0, 0};
const int Is[] PROGMEM = {31, 0, 0, 0, 0};
const int Js[] PROGMEM = {8, 16, 15, 0, 0};
const int Ks[] PROGMEM = {31, 4, 27, 0, 0};
const int Ls[] PROGMEM = {31, 16, 16, 0, 0};
const int Ms[] PROGMEM = {31, 2, 4, 2, 31};
const int Ns[] PROGMEM = {31, 4, 8, 31, 0};
const int Os[] PROGMEM = {31, 17, 31, 0, 0};
const int Ps[] PROGMEM = {31, 9, 15, 0, 0};
const int Qs[] PROGMEM = {31, 17, 15, 16, 0};
const int Rs[] PROGMEM = {31, 5, 27, 0, 0};
const int Ss[] PROGMEM = {23, 21, 29, 0, 0};
const int Ts[] PROGMEM = {1, 31, 1, 0, 0};
const int Us[] PROGMEM = {31, 16, 31, 0, 0};
const int Vs[] PROGMEM = {15, 16, 15, 0, 0};
const int Ws[] PROGMEM = {15, 16, 8, 16, 15};
const int Xs[] PROGMEM = {17, 10, 4, 10, 1};
const int Ys[] PROGMEM = {1, 2, 28, 2, 1};
const int Zs[] PROGMEM = {25, 21, 19, 0, 0};

const int _0s[] PROGMEM = {31, 17, 31, 0, 0};
const int _1s[] PROGMEM = {18, 31, 16, 0, 0};
const int _2s[] PROGMEM = {29, 21, 23, 0, 0};
const int _3s[] PROGMEM = {17, 21, 31, 0, 0};
const int _4s[] PROGMEM = {7, 4, 31, 0, 0};
const int _5s[] PROGMEM = {23, 21, 29, 0, 0};
const int _6s[] PROGMEM = {31, 21, 29, 0, 0};
const int _7s[] PROGMEM = {25, 5, 3, 0, 0};
const int _8s[] PROGMEM = {31, 21, 31, 0, 0};
const int _9s[] PROGMEM = {7, 5, 31, 0, 0};
///////////////////////////////

///////// Shift register interface //////////
#define		ser		PC0
#define		oeN		PC1
#define		rclk	PC2
#define		srclk	PC3
#define		srclrN	PC4

///////// RS485 direction control //////////
#define		dir		PB1

///////// Initialization functions //////////
void InitGPIO(void);
void InitTimer(void);
void InitUSART1(void);
void Init(void);

///////// Shift registers functions //////////
void TurnOffShiftRegister(void);
void TurnOnShiftRegister(void);
void ResetRegisters(void);
void ResetShiftRegisters(void);
void ClockFirstStage(void);
void ClockSecondStage(void);

///////// Font drawing functions //////////
void CreateMatrixBigFont(char*);
void CreateMatrixSmallFont(char*, char*);
int GetColumnBigFont(char, int);
int GetColumnSmallFont(char, int);
void DrawFrame(void);
void DrawRows(int);
void SetPin(int);
void ClearPin(int);
void ScrollControll(void);
void DrawFrameControll(void);
void MessageReceptionControll(void);

void PrintMyAddress(void);
void CreateMatrixBigFont(char*);
uint8_t ValidateMessage(uint8_t, char*, char*, char*, char*);

///////// Address related functions //////////
uint8_t ReadMyAddress(void);
bool CompareAddresses(uint8_t, char*);

///////// USART send byte functions //////////
//void USART0_send_byte(uint8_t);
//void USART1_send_byte(uint8_t);

volatile uint16_t matrix[200];
volatile uint8_t tot_overflow;
char message[70];

uint16_t broadcastAddress = 99;	// Broadcast address, all devices will respond to command with this address

char firstDigit, secondDigit;
char myAddress[4];

char address[3];			// Address of a targeted device
char message_first[30];		// First message, upper row of a display
char message_second[30];	// Second message, lower row of a display
char scroll[2];			// Scrolling text, true/false
bool message_complete_flag = false;	// Message complete flag

uint8_t message_counter = 0;
uint32_t scrollCounter = 0;
bool scrollingEnabled = false;

// Blinking
bool blinkEnable = false;
uint16_t blinkCounter = 0;
uint16_t blinkPeriod = 20000;
bool blinkToggle = false;

uint16_t scrollSpeed = 2500;		// Scroll speed
uint16_t brightness = 150;		// Display brightness

void InitGPIO()
{
	// Configures data direction of port pins as outputs
	DDRA = 0xFF;
	DDRB = 0xFF;
	DDRC = 0xFF;
	DDRE = 0xFF;
	
	// Configure data direction of port pins as inputs
	DDRD = 0x00;
	PORTD = 0x00;
	
	//DDRD &= ~(1<<PD2);    //Configure PORTD pin 2 as an input
	//PORTD &= ~(1<<PD2);    //Activate pull-ups in PORTD pin 2
}

void InitTimer()
{
	// set up timer with no prescaling
	TCCR0 |= (1 << CS00);
	
	// initialize counter
	TCNT0 = 0;
	
	// enable overflow interrupt
	TIMSK |= (1 << TOIE0);
	
	// initialize overflow counter variable
	tot_overflow = 0;
}

/*
void InitUSART0()
{
	uint32_t baudrate = (F_CPU / 16 / BAUD) - 1;
	UBRR0H = (baudrate >> 8);	// shift the register right by 8 bits
	UBRR0L = baudrate;		// set baud rate
	
	UCSR0A = 0;
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);   // Turn on the transmission and reception circuitry 
	UCSR0C = (1 << URSEL0) | (1 << UCSZ00) | (1 << UCSZ01); // Use 8-bit character sizes - URSEL bit set to select the UCRSC register
}
*/

void InitUSART1()
{
	uint32_t baudrate = (F_CPU / 16 / BAUD) - 1;
	UBRR1H = (baudrate >> 8);	// shift the register right by 8 bits
	UBRR1L = baudrate;		// set baud rate
	
	UCSR1A = 0;
	UCSR1B = (1 << RXEN1) | (1 << TXEN1) | (1 << RXCIE1);   // Turn on the transmission and reception circuitry
	UCSR1C = (1 << URSEL1) | (1 << UCSZ10) | (1 << UCSZ11); // Use 8-bit character sizes - URSEL bit set to select the UCRSC register
}

void Init()
{
	MCUCSR=(1<<JTD);	
	sei();
}

/*
void USART0_send_byte(uint8_t u8Data)
{
	// Wait until last byte has been transmitted
	while((UCSR0A &(1<<UDRE0)) == 0);
	
	// Transmit data
	UDR0 = u8Data;
}
*/

/*
void USART1_send_byte(uint8_t u8Data)
{
	// RS485 write enable
	//PORTB |= (1 << dir);	// Set pin high
	
	// Wait until last byte has been transmitted
	while((UCSR1A &(1<<UDRE1)) == 0);
	
	// Transmit data
	UDR1 = u8Data;
	
	//while(TXC1 != 0);
	
	// RS485 write disable
	//PORTB &= ~(1 << dir);	// Set pin low
	
	//UCSR1A = (0<<TXC1);
}
*/

ISR(TIMER0_OVF_vect)
{
	// keep a track of number of overflows
	tot_overflow++;
	
	if(blinkEnable == true)
	{
		blinkCounter++;
		
		if(blinkCounter <= blinkPeriod)
		{
			blinkToggle = true;
		}
		else if(blinkCounter > blinkPeriod && blinkCounter <= (blinkPeriod * 2))
		{
			blinkToggle = false;
		}
		else
		{
			blinkCounter = 0;
		}
	}
	else
	{
		blinkCounter = 0;
		blinkToggle = true;
	}
}

/*
ISR(INT0_vect)
{
	
}
*/

ISR(USART1_RXC_vect)
{	
	// UDR1
	char receivedByte;
	receivedByte = UDR1;
	
	if(receivedByte == '$')		// New message
	{
		message_counter = 0;	// Clear counter
	}
	
	if(receivedByte != '#')
	{
		message[message_counter] = receivedByte;
		message_counter++;
	}
	
	if(receivedByte == '#')		// End of the message
	{
		message[message_counter] = receivedByte;
		message_counter++;
		message_complete_flag = true;		
	}
}

void Process_message()
{
	/* Segment description */
	// 1. segment - Start message ($)
	// 2. segment - Device address (00 - 31)
	// 3. segment - First message (30 chars in length)
	// 4. segment - Second message (30 chars in length)
	// 5. segment - Scrolling control (S/N)
	// 6. segment - End message (#)	
	// | - Segment delimiter
	
	/* Examples */
	// $|12|QWER|TYUI|N|#             - two rows message, without scrolling
	// $|12|QWER|*|N|#                - one row message, without scrolling
	// $|12|QWER|TYUI|S|#             - two rows message, with scrolling
	// $|12|QWER|*|S|#                - one row message, with scrolling	
	// $|12|*|TYUI|S|#                - False message, lead to "ERR"
	// $|12|*|*|S|#					  - False message, lead to "ERR"
	// $|12|*|TYUI|N|#                - False message, lead to "ERR"
	// $|12|*|*|N|#					  - False message, lead to "ERR"
	
	// Address '99' - All devices will respond
	// Example
	// $|99|BREAK|*|S|#				  - All devices will show scrolling "BREAK" message
	
	// $|99|*|*|?|#					  - show my address
	// $|12|001-150|*|B|#			  - brightness
	// $|12|001-099|*|V|#			  - speed
	
	// error codes {broj delimitera, adrese, duzina teksta, pogresna kontrola, poruka bez kraja}
		
	// greska - kada se nakon ERR poruke posalje Scroll poruka ne skroluje
	
	
	uint8_t delimiter_counter = 0;
	uint8_t my_Address;
	bool address_match = false;
	
	char address_temp[3];			// Address of a targeted device
	char message_first_temp[30];		// First message, upper row of a display
	char message_second_temp[30];	// Second message, lower row of a display
	char scroll_temp[2];			// Scrolling text, true/false
	
	//uint8_t error_code;
	
	char *p = strtok(message, "|");
	
	while (p != NULL)
	{
		
		if(delimiter_counter == 1)
		{
			strcpy(address_temp, p);
		}
		
		if(delimiter_counter == 2)
		{
			strcpy(message_first_temp, p);
		}
		
		if(delimiter_counter == 3)
		{
			strcpy(message_second_temp, p);
		}
		
		if(delimiter_counter == 4)
		{
			strcpy(scroll_temp, p);
		}
		
		delimiter_counter++;
		p = strtok (NULL, "|");
	}
	
	message_complete_flag = false;	
	
	my_Address = ReadMyAddress();
	address_match = CompareAddresses(my_Address, address_temp);
	
	//error_code = ValidateMessage(delimiter_counter, address, message_first, message_second, scroll);
	
	if(address_match == true)
	{		
		strcpy(address, address_temp);
		strcpy(scroll, scroll_temp);
		
		if(strcmp(scroll, "S") == 0 || strcmp(scroll, "N") == 0)
		{
			strcpy(message_first, message_first_temp);
			strcpy(message_second, message_second_temp);
		
			if(strcmp(message_first, "*") != 0 && strcmp(message_second, "*") != 0)	// Print two row message
			{
				CreateMatrixSmallFont(message_first, message_second);
			}
			else
			{
				if(strcmp(message_first, "*") != 0)
				{
					if(strcmp(scroll, "B") != 0 && strcmp(scroll, "V") != 0)
					{
						CreateMatrixBigFont(message_first);	// Print one row message
					}
				}
			}
			
			if(strcmp(scroll, "S") == 0)
			{	
				scrollingEnabled = true;
			}
			if(strcmp(scroll, "N") == 0)
			{
				scrollingEnabled = false;
			}
		}
		else
		{
			if(strcmp(scroll, "?") == 0)
			{
				scrollingEnabled = false;
				PrintMyAddress();
				CreateMatrixBigFont(myAddress); // Show my address
			}
		
			if(strcmp(scroll, "B") == 0)
			{
				brightness = (message_first_temp[0] - '0') * 100 + (message_first_temp[1] - '0') * 10 + (message_first_temp[2] - '0');
			}
		
			if(strcmp(scroll, "V") == 0)
			{
				scrollSpeed = (((message_first_temp[0] - '0') * 100 + (message_first_temp[1] - '0') * 10 + (message_first_temp[2] - '0'))) * 10 ;
			}
			
			
			if(strcmp(scroll, "F") == 0)	// Start blinking
			{
				blinkEnable = true;
			}
			
			if(strcmp(scroll, "G") == 0)	// Stop blinking
			{
				blinkEnable = false;
			}
			
		}
	}
}

uint8_t ValidateMessage(uint8_t delimiter_counter_temp, char *address_temp, char *message_first_temp, char *message_second_temp, char *scroll_temp)
{
	// ERR1 - First message length error
	// ERR2 - Second message length error
	// ERR3 - False command
	
	uint8_t answer = 0;
	
	if(strlen(message_first_temp) > 30 || strlen(message_first_temp) == 0 || strlen(message_second_temp) > 30 || strlen(message_second_temp) == 0 || strlen(message_second_temp) > 30 || strlen(message_second_temp) == 0)
	{
		answer = 1;
	}

	if(strcmp(scroll, "S") != 0 && strcmp(scroll, "N") != 0 && strcmp(scroll, "?") != 0 && strcmp(scroll, "B") != 0 && strcmp(scroll, "V") != 0)
	{
		answer = 2;
	}
	
	return answer;
}

uint8_t ReadMyAddress()
{
	int temp_adr = 0;
	
	if (PIND & (1<<PD0))
		temp_adr += 1;
	if (PIND & (1<<PD1))
		temp_adr += 2;
	if (PIND & (1<<PD2))
		temp_adr += 4;
	if (PIND & (1<<PD3))
		temp_adr += 8;
	if (PIND & (1<<PD4))
		temp_adr += 16;
		
	return temp_adr;
}

void PrintMyAddress()
{
	firstDigit = ReadMyAddress() / 10 + '0';
	secondDigit = ReadMyAddress() % 10 + '0';
	
	myAddress[0] = ' ';
	myAddress[1] = firstDigit;
	myAddress[2] = secondDigit;
	myAddress[3] = ' ';
}

// deviceAddress - address read from the board (dip-switch)
// messageAddress - address read from a message received
bool CompareAddresses(uint8_t deviceAddress, char *messageAddress)
{
	uint8_t addr = (messageAddress[0] - '0') * 10 + (messageAddress[1] - '0');
	
	if(addr == deviceAddress || addr == broadcastAddress)
	{
		return true;
	}
	else
	{
		return false;
	}
}

// Turn off shift registers (outputs are three-stated)
void TurnOffShiftRegister()
{
	if(blinkToggle == true)
	{
		PORTC |= (1 << oeN);	// Set pin high
		//_delay_us(1);
	}
	else
	{
		PORTC |= (1 << oeN);	// Set pin high
		_delay_us(1);
	}
}

// Turn on shift registers (output depend on the row definition)
void TurnOnShiftRegister()
{
	if(blinkToggle == true)
	{
		PORTC &= ~(1 << oeN);	// Set pin low
		_delay_us(1);	
	}
	else
	{
		PORTC |= (1 << oeN);	// Set pin high
		_delay_us(1);
	}
}

// Reset registers to their default state
void ResetRegisters()
{
	PORTC &= ~(1 << srclrN);	// Set pin low
	PORTC |= (1 << srclrN);	// Set pin high
}

// Clock the first stage of the register
void ClockFirstStage()
{
	PORTC |= (1 << srclk);	// Set pin high
	PORTC &= ~(1 << srclk);	// Set pin low
}

// Clock the second stage of the register
void ClockSecondStage()
{
	PORTC |= (1 << rclk);	// Set pin high
	PORTC &= ~(1 << rclk);	// Set pin low
}

// Reset shift registers
void ResetShiftRegisters()
{
	TurnOffShiftRegister();
	ResetRegisters();
	ClockSecondStage();
	TurnOnShiftRegister();
}

void CreateMatrixBigFont(char *str)
{
	uint8_t columnCounter = 0;
	uint8_t i, j;
	
	strcat(str, "    ");
	
	for (i = 0; i < 200; i++)
	{
		matrix[i] = 0;
	}
	
	for(i = 0; i < strlen(str); i++)
	{
		for(j = 0; j < charWidth; j++)
		{
			matrix[columnCounter] = GetColumnBigFont(str[i], j) << big_font_offset;
			
			if(matrix[columnCounter] != 0 || str[i] == ' ')
			{
				columnCounter++;
			}
		}
		matrix[columnCounter] = 0x00;
		columnCounter++;
	}
	
	for(i = columnCounter; i < frameWidth; i++)
	{
		matrix[i] = 0x00;
	}
}

void CreateMatrixSmallFont(char *message_1, char *message_2)
{
	uint8_t columnCounter = 0;
	uint8_t i, j, columnPicker;
	
	for (i = 0; i < 200; i++)
	{
		matrix[i] = 0;
	}
	
	for (i = 0; i < strlen(message_1); i++)
	{
		for (j = 0; j < charWidth; j++)
		{
			matrix[columnCounter] = GetColumnSmallFont(message_1[i], j) << small_font_first_row_offset;
			if(matrix[columnCounter] != 0)
			{
				columnCounter++;
			}
		}
		matrix[columnCounter] = 0x00;
		columnCounter++;
	}
	
	for(i = columnCounter; i < frameWidth; i++)
	{
		matrix[i] = 0x00;
	}
	
	columnCounter = 0;
	
	for (i = 0; i < strlen(message_2); i++)
	{
		for (j = 0; j < charWidth; j++)
		{
			columnPicker = GetColumnSmallFont(message_2[i], j);
			matrix[columnCounter] |= columnPicker << small_font_second_row_offset;
			if(columnPicker != 0)
			{
				columnCounter++;
			}
		}
		matrix[columnCounter] |= 0x00 << small_font_second_row_offset;
		columnCounter++;
	}
	
	for(i = columnCounter; i < frameWidth; i++)
	{
		matrix[i] |= 0x00 << small_font_second_row_offset;
	}
}

void ScrollMatrix(bool enableScrolling)
{
	int temp, i;
	
	if(enableScrolling)
	{
		temp = matrix[0];
	
		for(i = 0; i < (strlen(message_first) * charWidth + strlen(message_first) - 1); i++)
		{
			matrix[i] = matrix[i+1];
		}
	
		matrix[strlen(message_first) * charWidth + strlen(message_first) - 1] = temp;
	}
}

int GetColumnBigFont(char c, int i)
{
	int elements[charWidth];

	switch(c)
	{
		case 'A':	memcpy_P(elements, A, 10); break;
		case 'B':	memcpy_P(elements, B, 10); break;
		case 'C':   memcpy_P(elements, C, 10); break;
		case 'D':   memcpy_P(elements, D, 10); break;
		case 'E':   memcpy_P(elements, E, 10); break;
		case 'F':   memcpy_P(elements, F, 10); break;
		case 'G':   memcpy_P(elements, G, 10); break;
		case 'H':   memcpy_P(elements, H, 10); break;
		case 'I':   memcpy_P(elements, I, 10); break;
		case 'J':   memcpy_P(elements, J, 10); break;
		case 'K':   memcpy_P(elements, K, 10); break;
		case 'L':   memcpy_P(elements, L, 10); break;
		case 'M':   memcpy_P(elements, M, 10); break;
		case 'N':   memcpy_P(elements, N, 10); break;
		case 'O':   memcpy_P(elements, O, 10); break;
		case 'P':   memcpy_P(elements, P, 10); break;
		case 'Q':   memcpy_P(elements, Q, 10); break;
		case 'R':   memcpy_P(elements, R, 10); break;
		case 'S':   memcpy_P(elements, S, 10); break;
		case 'T':   memcpy_P(elements, T, 10); break;
		case 'U':   memcpy_P(elements, U, 10); break;
		case 'V':   memcpy_P(elements, V, 10); break;
		case 'W':   memcpy_P(elements, W, 10); break;
		case 'X':   memcpy_P(elements, X, 10); break;
		case 'Y':   memcpy_P(elements, Y, 10); break;
		case 'Z':   memcpy_P(elements, Z, 10); break;
			
		case '0':   memcpy_P(elements, _0, 10); break;
		case '1':   memcpy_P(elements, _1, 10); break;
		case '2':   memcpy_P(elements, _2, 10); break;
		case '3':   memcpy_P(elements, _3, 10); break;
		case '4':   memcpy_P(elements, _4, 10); break;
		case '5':   memcpy_P(elements, _5, 10); break;
		case '6':   memcpy_P(elements, _6, 10); break;
		case '7':   memcpy_P(elements, _7, 10); break;
		case '8':   memcpy_P(elements, _8, 10); break;
		case '9':   memcpy_P(elements, _9, 10); break;
			
		case '>':   memcpy_P(elements, rightArrow, 10); break;
		case '<':   memcpy_P(elements, leftArrow, 10); break;
		case '^':   memcpy_P(elements, upArrow, 10); break;
		case '_':   memcpy_P(elements, downArrow, 10); break;
			
		case ' ':   memcpy_P(elements, space, 10); break;
	}
	
	return elements[i];
}

int GetColumnSmallFont(char c, int i)
{
	int elements[charWidth];

	switch(c)
	{
		case 'A':	memcpy_P(elements, As, 10); break;
		case 'B':	memcpy_P(elements, Bs, 10); break;
		case 'C':   memcpy_P(elements, Cs, 10); break;
		case 'D':   memcpy_P(elements, Ds, 10); break;
		case 'E':   memcpy_P(elements, Es, 10); break;
		case 'F':   memcpy_P(elements, Fs, 10); break;
		case 'G':   memcpy_P(elements, Gs, 10); break;
		case 'H':   memcpy_P(elements, Hs, 10); break;
		case 'I':   memcpy_P(elements, Is, 10); break;
		case 'J':   memcpy_P(elements, Js, 10); break;
		case 'K':   memcpy_P(elements, Ks, 10); break;
		case 'L':   memcpy_P(elements, Ls, 10); break;
		case 'M':   memcpy_P(elements, Ms, 10); break;
		case 'N':   memcpy_P(elements, Ns, 10); break;
		case 'O':   memcpy_P(elements, Os, 10); break;
		case 'P':   memcpy_P(elements, Ps, 10); break;
		case 'Q':   memcpy_P(elements, Qs, 10); break;
		case 'R':   memcpy_P(elements, Rs, 10); break;
		case 'S':   memcpy_P(elements, Ss, 10); break;
		case 'T':   memcpy_P(elements, Ts, 10); break;
		case 'U':   memcpy_P(elements, Us, 10); break;
		case 'V':   memcpy_P(elements, Vs, 10); break;
		case 'W':   memcpy_P(elements, Ws, 10); break;
		case 'X':   memcpy_P(elements, Xs, 10); break;
		case 'Y':   memcpy_P(elements, Ys, 10); break;
		case 'Z':   memcpy_P(elements, Zs, 10); break;
		
		case '0':   memcpy_P(elements, _0s, 10); break;
		case '1':   memcpy_P(elements, _1s, 10); break;
		case '2':   memcpy_P(elements, _2s, 10); break;
		case '3':   memcpy_P(elements, _3s, 10); break;
		case '4':   memcpy_P(elements, _4s, 10); break;
		case '5':   memcpy_P(elements, _5s, 10); break;
		case '6':   memcpy_P(elements, _6s, 10); break;
		case '7':   memcpy_P(elements, _7s, 10); break;
		case '8':   memcpy_P(elements, _8s, 10); break;
		case '9':   memcpy_P(elements, _9s, 10); break;
		
		case ' ':   memcpy_P(elements, space, 10); break;
		
	}

	return elements[i];
}

void DrawFrame()
{
	int i;
	
	TurnOffShiftRegister();
	ResetRegisters();
	
	PORTC |= (1 << ser);	// Set pin high
	ClockFirstStage();
	PORTC &= ~(1 << ser);	// Set pin low
	ClockSecondStage();
	
	for(i = 0; i < frameWidth; i++)
	{
		DrawRows(matrix[i]);
		
		ClockFirstStage();
		ClockSecondStage();
	}
}

void DrawRows(int r)
{
	int i, b;
	
	TurnOffShiftRegister();
	
	for(i = 0; i <= displayHeigth; i++)
	{
		b = r % 2;
		r = r >> 1;
		if(b == 1)
		{
			ClearPin(i);
		}
		else
		{
			SetPin(i);
		}
	}
	
	TurnOnShiftRegister();
	
	for (int i = 0; i < brightness; i++)
	{
		_delay_us(1);
	}
	
	for(i = 0; i <= displayHeigth; i++)
	{
		SetPin(i);
	}
	
	TurnOffShiftRegister();
	
	for (int i = 0; i < (150 - brightness); i++)
	{
		_delay_us(1);
	}
}

void SetPin(int i)
{
	switch(i)
	{
		case 0:	PORTC |= (1 << 5);	break;
		case 1:	PORTC |= (1 << 6);	break;
		case 2:	PORTC |= (1 << 7);	break;
		case 3:	PORTE |= (1 << 2);	break;
		case 4:	PORTE |= (1 << 1);	break;
		case 5:	PORTE |= (1 << 0);	break;
		case 6:	PORTA |= (1 << 7);	break;
		case 7:	PORTA |= (1 << 6);	break;
		case 8:	PORTA |= (1 << 5);	break;
		case 9:	PORTA |= (1 << 4);	break;
		case 10:	PORTA |= (1 << 3);	break;
	}
}

void ClearPin(int i)
{
	switch(i)
	{
		case 0:	PORTC &= ~(1 << 5);	break;
		case 1:	PORTC &= ~(1 << 6);	break;
		case 2:	PORTC &= ~(1 << 7);	break;
		case 3:	PORTE &= ~(1 << 2);	break;
		case 4:	PORTE &= ~(1 << 1);	break;
		case 5:	PORTE &= ~(1 << 0);	break;
		case 6:	PORTA &= ~(1 << 7);	break;
		case 7:	PORTA &= ~(1 << 6);	break;
		case 8:	PORTA &= ~(1 << 5);	break;
		case 9:	PORTA &= ~(1 << 4);	break;
		case 10:	PORTA &= ~(1 << 3);	break;
	}
}

void ScrollControll()
{
	if(scrollCounter >= scrollSpeed)
	{
		ScrollMatrix(scrollingEnabled);	// Enable scrolling
		scrollCounter = 0;
	}
	scrollCounter++;
}

void DrawFrameControll()
{
	if(tot_overflow >= 50)
	{
		DrawFrame();
		tot_overflow = 0;
	}
}

void MessageReceptionControll()
{
	if(message_complete_flag == true)
	{
		Process_message();
	}
}

int main(void)
{			
	InitGPIO();
	InitTimer();
	InitUSART1();
	Init();
	
	PORTB &= ~(1 << dir);	// Set pin low, enable reception
	
	PrintMyAddress();
	CreateMatrixBigFont(myAddress); // Show my address
	
    while (1) 
    {
		ScrollControll();		
		DrawFrameControll();
		MessageReceptionControll();
    }
}